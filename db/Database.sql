-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 08 Décembre 2020 à 00:05
-- Version du serveur :  5.5.55-0+deb8u1
-- Version de PHP :  7.2.26-1+0~20191218.33+debian8~1.gbpb5a34b

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `Pr-inpt`
--

-- --------------------------------------------------------

--
-- Structure de la table `personnage`
--

CREATE TABLE IF NOT EXISTS `personnage` (
`id` int(11) NOT NULL,
  `name` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `characteristics` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `personnage`
--

INSERT INTO `personnage` (`id`, `name`, `photo`, `characteristics`) VALUES
(1, 'Kratos', 'https://i.pinimg.com/originals/c2/fe/a2/c2fea237bb8588b6cac7046d7c89dca2.png', 'Blade Of Chaos'),
(2, 'Zeus', 'https://pngimage.net/wp-content/uploads/2018/06/%D0%B7%D0%B5%D0%B2%D1%81-png-4.png', 'Blade Of Olympus'),
(3, 'Thor', 'https://lh3.googleusercontent.com/proxy/iYMKpWqxTcuyjfqK-FDaSMdSc1BNLYdOsAFjJdxlUH76kmOeOtpZ5swJT15S9lcbmYPaqU2Kis_nW1Nx94-l13tnrRm9wJqY', 'Mjolnir');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `personnage`
--
ALTER TABLE `personnage`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `personnage`
--
ALTER TABLE `personnage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

  // Test de lecture
var mysql = require('mysql');
  test('Database lecture', () => {
    var connection = mysql.createConnection({
      host     : '172.16.238.3',
      port : 3306,
      user     : 'root',
      password : 'rootroot',
      database : 'Database'
    });
     connection.connect(function(err) {
      if (err) throw err
      connection.query("SELECT * FROM personnage", function (err, result, fields) {
        if (err) throw err;
        expect(result[0].name).toBe("Kratos"); 
      });
    })
  });

  // Test d'ecriture
  test('Database ecriture', () => {
    var connection = mysql.createConnection({
      host     : '172.16.238.3',
      port : 3306,
      user     : 'root',
      password : 'rootroot',
      database : 'Database'
    });
    connection.connect(function(err) {
      if (err) throw err
      var sql = "INSERT INTO personnage (name, photo,characteristics) VALUES ?"
    var value = [["Nom de test","Photo de test","Carac de test"]];
    connection.query(sql,[value], function (err, result) {
      if (err) throw err;
    });
    connection.query("SELECT * FROM personnage", function (err, result, fields) {
      if (err) throw err;
      expect(result[result.length-1].name).toBe("Nom de test"); 
    });
    connection.query("DELETE FROM personnage where name = 'Nom de test'", function (err, result, fields) {
      if (err) throw err;
    });
    });
  });





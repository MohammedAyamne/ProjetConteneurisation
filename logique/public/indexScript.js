const TitleDiv = document.getElementById("renderTitleDiv");
const ContentDiv = document.getElementById("main");
console.log(ContentDiv);
axios
  .get("/personnage")
  .then((res) => {
    let personnage = res.data;
    for (var i = 0; i < personnage.length; i++) {
      let renderedHTML = `<li><a href="#about${i}"><i class="bx bx-user"></i> <span>${personnage[i].name}</span></a></li>`;
      TitleDiv.innerHTML += renderedHTML;
      let renderedHTML2 = `  <!-- ======= About Section ======= -->
      <section id="about" class="about" id="renderContentDiv">
        <div class="container">
          <div class="section-title">
            <h2>${personnage[i].name}</h2>
          </div>
          <div class="row">
            <div class="col-lg-4" data-aos="fade-right">
              <img src="${personnage[i].photo}" class="img-fluid" alt="">
            </div>
            <div class="col-lg-8 pt-4 pt-lg-0 content" data-aos="fade-left">
              <h3>Caractéristique</h3>
              <p>
              ${personnage[i].characteristics}
              </p>
            </div>
          </div>
  
        </div>
      </section><!-- End About Section -->`;
      ContentDiv.innerHTML += renderedHTML2;
    }
  })
  .catch((error) => {
    console.log(error);
  });

  






document.getElementById("addCharacter")
  .addEventListener("click", () => {
      try {
          (async () => {
            const res = await axios.post("/personnage", {
              name: document.getElementById("name").value,
              photo: document.getElementById("photo").value,
              caracteristique: document.getElementById("carac").value,
            });
          })();
          location.reload();
      } catch (error) {
      }
  });
